# mongodb

Implementacao na instalaçao do mongodb

A instalaçao do MongoDB pode ser realizada para ser utilizado em container (a imagem encontra-se em https://gitlab.com/desafio_devsecops_maxmilhas/images) ou em uma VM ou maquina fisica.
Para instalacao em maquina fisica, o serviço deve ser inicializado, alterando o parametro init_service no arquivo de configuraçao do playbook.

Exemplo:
```python
mongodb:
  init_service: 'yes' #inicializa o serviço, utilizado quando o mongo e instalado em VM ou em maquina fisica
```

Para impedir que o servico seja inicializado, altere para no, conforme abaixo:
```python
mongodb:
  init_service: 'no' #apenas instala o mongo, as configuraçoes ficam default
```

E possivel alterar onde o mongo ira salvar os dados e onde o log sera salvo

Exemplo:
```python
mongodb:
  data_path: /opt/data/db #alterando o local de onde os dados serao salvos
  log_path: /opt/mongodb/logs #alterando o local de onde o log sera salvo
```




